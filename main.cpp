//EXPRESIONES
//ejercicio 4
//escribe la siguiente expresion como expresion en c++: (a/b)+1
//ejercicio 5
//escribe la siguiente expresion como expresion en c++: sqrt(a-d/c)-(a/b-b)
//ejercicio 6
//escriba un programa que lea de la entrada estandar 2 catetos de un triangulo rectangulo  y escrib en la salida
//su hipotenusa(5mins)
//ejercicio 7
//la calificacion final de un estudiante es la media ponderado de 3 notas: nota de practicas que cuenta 30% del total
//la nota teorica cuenta con el 60% del total, y el 10% queda sobre la participacion. Escriba un programa
//que lea las 3 notas de un alumno y se muestre como salida la nota final
#include <iostream>
#include <math.h>
using namespace std;
int main()
{
    //ejercicio 4
    /*float a,b,resul=0;
    cout<<"ingrese el valor de a: ";cin>>a;
    cout<<"ingrese el valor de b: ";cin>>b;
    resul=(a/b)+1;
    cout.precision(2);
    cout<<"el resultado es: "<<resul<<endl;*/
    //ejercicio 5
    /*float a,b,c,d,resul=0;
    cout<<"ingrese el valor de a: ";cin>>a;
    cout<<"ingrese el valor de b: ";cin>>b;
    cout<<"ingrese el valor de c: ";cin>>c;
    cout<<"ingrese el valor de d: ";cin>>d;
    resul=sqrt(a-d/c)-(a/b-b);
    cout<<"el resultado es: "<<resul;*/
    //ejercicio 6
    /*float a,b,c;
    cout<<"escriba los catetos del triangulo rectangulo:"<<endl;
    cin>>a>>b;
    c=sqrt((a*a)+(b*b));
    cout.precision(2);
    cout<<"la hipotenusa es: "<<c<<endl;*/
    //ejercicio 7
    float n1,n2,n3,media=0;
    cout<<"ingrese la nota de practicas: ";cin>>n1;
    cout<<"ingrese la nota teorica: ";cin>>n2;
    cout<<"ingrese la nota de participacion: ";cin>>n3;
    cout<<"30% de la nota practicas: "<<n1*0.3<<endl;
    cout<<"60% de la nota teorica: "<<n2*0.6<<endl;
    cout<<"10% de la nota participacion: "<<n3*0.1<<endl;
    media=(n1+n2+n3)/3;
    cout<<"la calificacion final del alumno es: "<<media<<endl;
    return 0;
}
